from django.views.generic.list import ListView
from todos.models import TodoList, TodoItem
from django.shortcuts import render

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todos"
    paginate_by = 8
